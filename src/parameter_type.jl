import Base: getindex, endof, length, size, start, done, next, setindex!, push!, append!

abstract type AbstractParameter{T} <: AbstractVector{T} end
abstract type AbstractParameterCollection{T} <: AbstractVector{T} end

struct Param{T} <: AbstractParameter{T}
    param::Vector{T}
    odetype::DataType
    cost::Float64
    tshift::Float64
    metadata::Dict{Symbol, Any}
end

struct ParamCollection{T} <: AbstractParameterCollection{T}
    parameters::Vector{Param{T}}
end


getindex(p::AbstractParameter, args...) = getindex(p.param, args...)
setindex!(p::AbstractParameter, args...) = setindex!(p.param, args...)
endof(p::AbstractParameter) = endof(p.param)
length(p::AbstractParameter) = length(p.param)
size(p::AbstractParameter) = size(p.param)
start(p::AbstractParameter) = start(p.param)
next(p::AbstractParameter, args...; kwargs...) = next(p.param, args...; kwargs...)
done(p::AbstractParameter, args...) = done(p.param, args...)

getindex(p::AbstractParameterCollection, args...) = getindex(p.parameters, args...)
getindex(p::AbstractParameterCollection, ::Colon, j::Integer) = [getindex(i.param, j) for i in p]
# getindex(p::AbstractParameterCollection, j::Integer, ::Colon) = [getindex(i.param, j) for i in p]
getindex(p::AbstractParameterCollection, j::Integer, ::Colon) = [getindex(i.param, j) for i in p]
getindex(p::AbstractParameterCollection, ::Colon, ::Colon) = vcat([i.param' for i in p]...)
getindex(p::AbstractParameterCollection, r1::AbstractArray, r2::AbstractArray) = vcat([p[i].param[r1]' for i in r2]...)
# getindex(p::AbstractParameterCollection, r::AbstractUnitRange, ::Colon) = vcat([p[i].param' for i in r]...)
# getindex(p::AbstractParameterCollection, j::Integer, r::AbstractUnitRange) = [getindex(p[i].param, j) for i in r]

getindex(p::AbstractParameterCollection, i::Integer, j::Integer )= getindex(p[i].param, j)
endof(p::AbstractParameterCollection) = endof(p.parameters)
start(p::AbstractParameterCollection) = start(p.parameters)
next(p::AbstractParameterCollection, args...) = next(p.parameters, args...)
done(p::AbstractParameterCollection, args...) = done(p.parameters, args...)
length(p::AbstractParameterCollection) = length(p.parameters)
size(p::AbstractParameterCollection) = size(p.parameters)

function push!(pars::AbstractParameterCollection, p::AbstractParameter; verbose=true)
    if p in pars
        verbose && warn("Parameter already in ParameterCollection, ignoring push! operation.")
    else
        push!(pars.parameters, p)
    end
    return nothing
end


function append!(pars::AbstractParameterCollection, pars2::AbstractParameterCollection)
    ### This is inefficient, but it does ensure that we do not duplicate params.
    for p in pars2
        push!(pars, p; verbose=false)
    end
end


(p::ParamCollection)(ode::AbstractReactionNetwork) = ParamCollection([i for i in p if ode isa i.odetype])
(p::ParamCollection)(ode::AbstractReactionNetwork, cost::Float64) = (p::ParamCollection)(ode, (-Inf, cost))
function (p::ParamCollection)(ode::AbstractReactionNetwork, cost::Tuple{Float64, Float64})
    [i for i in p if ode isa i.odetype && cost[1] <= i.cost <= cost[2]]
end

cost(par::AbstractParameterCollection) = getfield.(par, :cost)
loss(par::AbstractParameterCollection) = [p.metadata[:cost][:loss] for p in par]

function best_param(p, ode)
    minind = 1
    mincost = Inf
    for i in 1:length(p)
        if p[i].cost < mincost
            minind = copy(i)
            mincost = copy(p[i].cost)
        end
    end
    return p[minind]
end


function filter_params(par::AbstractParameterCollection, ode::ODEType;
    cost_min=-Inf, cost_max=Inf, loss=:any, tshift=:any, pamp=:any)

    filtered_param = ParamCollection([p for p in par if
            ode isa p.odetype &&
            cost_min <= p.cost <= cost_max &&
            (loss == :any || loss == p.metadata[:cost][:loss] ) &&
            (tshift == :any || tshift == p.tshift) &&
            (pamp == :any || pamp == p.metadata[:cost][:pamp] )
        ])
    # bitmap = BitVector([
    #     ode isa p.odetype &&
    #     cost_min <= p.cost <= cost_max &&
    #     (loss == p.metadata[:cost][:loss] || loss == :any) &&
    #     p.tshift == tshift
    #     for p in par]
    # )
    #
    # return @view par[bitmap]
    return filtered_param
end

param_matrix(par) = hcat((p for p in par)...)
