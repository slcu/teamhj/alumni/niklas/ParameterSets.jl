# ParameterSets.jl

This package is aimed at supplying a type system and file IO for parameters and parameter sets. A key feature is the preservation of information for reproducibility. The Parameter type stores metadata on what model it was generated for, and what optimisation procedure and parameters were used.


This package is in a very early stage and I would not yet recommend its general use. 
